To init a table in DB - run command:

*with default params:*

```
npm run initTable
```

*with custom params:*
```
node init_table.js <table name> <path to csv file>
```


------------


To start the Server - run commnad:

```
npm start
```
