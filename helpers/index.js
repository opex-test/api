const format = require("pg-format");
const _ = require('lodash');

// converting children props from object to array
const convertChildren = function (obj) {
  if (obj.children && typeof obj.children === "object") {
    let children = Object.values(obj.children);
    let convertedChildren = [];
    children.map(child => convertedChildren.push(convertChildren(child)));
    return {name: obj.name, size: obj.size, children: convertedChildren}
  } else {
    return obj;
  }
};

//creating object without arrays
const generateDependencyObj = function (rows, props) {
  let object = {};
  const {masterCircle, parentCircle, childrenCircle, parentSize, childrenSize, parentTooltip, childrenTooltip} = props;
  _.each(rows, (row) => {

    const childName = row[childrenCircle];
    const parentName = row[parentCircle];
    const masterName = row[masterCircle];

    const prevChildren = _.get(object, `${masterName}.children.${parentName}.children`, {});
    const prevParents = _.get(object, `${masterName}.children`, {});

    const child = {[childName]: {name: row[childrenTooltip], size: row[childrenSize]}};
    const newChildren = {...prevChildren, ...child };

    const parent = {[parentName]: {name: row[parentTooltip], size: row[parentSize], children: newChildren}};
    const newParents = {...prevParents, ...parent };

    object[masterName] = {name: row[masterCircle], children: newParents};
  });
  return object;
};

// getting properties from file
const getProperties = function (json, masterKey) {
  return new Promise((resolve, reject) => {
    const tableName = _.get(json, `${masterKey}[0].table`);
    const masterCircle = _.get(json, `${masterKey}[0]["master circle"]`);
    const parentCircle = _.get(json, `${masterKey}[0]["parent circle"]`);
    const childrenCircle = _.get(json, `${masterKey}[0]["children circle"]`);
    const parentSize = _.get(json, `${masterKey}[0]["parent size"]`);
    const childrenSize = _.get(json, `${masterKey}[0]["children size"]`);
    const parentTooltip = _.get(json, `${masterKey}[0]["parent tooltip"]`);
    const childrenTooltip = _.get(json, `${masterKey}[0]["children tooltip"]`);

    if (!tableName || !masterCircle || !parentCircle || !childrenCircle || !parentSize || !childrenSize || !parentTooltip || !childrenTooltip) {
      reject('Wrong JSON format');
    }
    resolve({tableName, masterCircle, parentCircle, childrenCircle, parentSize, childrenSize, parentTooltip, childrenTooltip})

  })
};

const selectQuery = function (props){
  const {tableName, masterCircle, parentCircle, childrenCircle, parentSize, childrenSize, parentTooltip, childrenTooltip} = props;
  return format(
    `SELECT %s, %s, %s, %s, %s, %s, %s FROM %s;`,
    masterCircle, parentCircle, childrenCircle, parentSize, childrenSize, parentTooltip, childrenTooltip, tableName
  );
};

const createTableQuery = function (tableName, params){
  let paramsQuery = '';
  params.map( param => paramsQuery += `${param} varchar(40),`);
  paramsQuery = paramsQuery.slice(0, -1) ;
  return `CREATE TABLE ${tableName} (${paramsQuery});`;
};

const insertToTableQuery = function (tableName, params, data){
  return format(`INSERT INTO %s (%s) VALUES %L`, tableName, params, data);
};


module.exports = {
  convertChildren,
  generateDependencyObj,
  getProperties,
  selectQuery,
  createTableQuery,
  insertToTableQuery
};
