const { Client } = require('pg');
const {
  convertChildren,
  getProperties,
  generateDependencyObj,
  selectQuery
} = require('../helpers');

const config = require('../config.json');
module.exports = async function (req, res, next) {

  // the checking that a file is exist and have correct JSON format
  if (!req.files || !req.files.configFile) { return res.status(400).send({error: 'file not found', data: null}) }
  let json = {};
  try {
    json = JSON.parse(req.files.configFile.data.toString())
  } catch (error) {
    return res.status(400).send({error: 'Wrong file format', data: null})
  }

  const masterKey = 'loadMaster'; // name of the head key in JSON file

  getProperties(json, masterKey) // getting properties from file
    .then(props => {
      const client = new Client(config.bd);
      client.connect( error => {
        if (error) { return res.status(500).send({error, data: null}) }

        client.query(selectQuery(props)) // getting data by our props from DB
          .then( data => {
            client.end();
            //creating object without arrays
            const dependencyObj = generateDependencyObj(data.rows, props);
            // converting children props from object to array
            const result = convertChildren({name: masterKey, children: dependencyObj});

            return res.status(200).send({data: result});
          })
          .catch( error => {
            return res.status(400).send({error, data: null})
          });
      })
    })
    .catch(error => { return res.status(400).send({error, data: null}) });
};