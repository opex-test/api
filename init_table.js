const { Client } = require('pg');
const csv = require('csvtojson');
const helpers = require('./helpers');
const config = require('./config.json');

const client = new Client(config.bd);
client.connect();

const tableName = process.argv[2] || "shipments_data";
const csvFile = process.argv[3] || "ShipmentData.csv";

csv({output: "csv", noheader: true})
  .fromFile(csvFile)
  .then( file => {
    const tableHead = file[0];
    const tableData = file.splice(1);

    client.query(helpers.createTableQuery(tableName, tableHead))
      .then(() => {
        client.query(helpers.insertToTableQuery(tableName, tableHead, tableData))
          .then(() => {
            client.end();
            console.log(`Table ${tableName} has created`)
          })
          .catch(err => {
            console.error(__filename, err);
            client.end();
          });
      })
      .catch(err => {
        console.error(__filename, err);
        client.end();
      });

  })
  .catch(err => {
    console.error(err);
    client.end();
  });
