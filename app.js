const express = require('express');
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');

const routes = require('./routes');

const app = express();

app.use(fileUpload({
  limits: { fileSize: 50 * 1024 * 1024 },
}));
app.use(bodyParser.json());

app.use('/api', routes);


app.listen(5500, function() {
  console.log('Listening on port 5500...')
});